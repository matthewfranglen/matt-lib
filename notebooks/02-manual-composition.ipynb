{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook I hope to implement dropout/dropconnect and weight decay.\n",
    "These two techniques are part of regularization and are conceptually quite simple.\n",
    "\n",
    "## Regularization\n",
    "\n",
    "[Regularization](https://en.wikipedia.org/wiki/Regularization_%28mathematics%29) is adding additional information which can cause the model to reduce overfitting.\n",
    "\n",
    "![regularized function](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Regularization.svg/354px-Regularization.svg.png)\n",
    "\n",
    "For this data set both curves have zero loss.\n",
    "Regularization can encourage the model to choose the green line, which is likely to generalize better (as it is less extreme).\n",
    "\n",
    "### Parameters/Weights and Activations\n",
    "\n",
    "In this I am going to talk about parameters, weights and activations.\n",
    "It is important to understand the difference.\n",
    "\n",
    " * Parameters - values used by a layer to change the input into the output\n",
    " * Weights - *same as parameters*, sometimes used to separate the changes that are dependent on the input from the bias parameters of the layer\n",
    " \n",
    " * Activation - values that pass through the network - at the beginning they are the input, at the end they are the output\n",
    "\n",
    " * Activation function - a kind of layer that typically has no parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dropout\n",
    "\n",
    "![dropout](images/dropout.png)\n",
    "\n",
    "Dropout involves setting some of the input activations for a layer to zero.\n",
    "The aim is to prevent a single activation from \"learning\" the overall output of a network for a given input (i.e. overfitting).\n",
    "Instead this knowledge needs to be spread across the activations, and no single activation can perform one classification.\n",
    "\n",
    "Dropout is typically set to 50%, and can be even higher, so it has a big impact on what the network can depend on to learn.\n",
    "\n",
    "### Random Forests\n",
    "\n",
    "Another way to think about dropout is by comparison to random forests.\n",
    "\n",
    "A decision tree takes the full set of inputs and uses them to produce a classification.\n",
    "When training random forests, each decision tree in the forest receives a subset of the input features, however it must still produce the desired classification.\n",
    "By combining the different decision trees in the forest a better classification system is produced as the classification that is chosen is the classification most consistently chosen for the different feature subsets.\n",
    "\n",
    "A neural network is a decision tree - it takes a set of inputs and produces a classification.\n",
    "When you apply dropout you are making a different network - one where it can only use a subset of the features.\n",
    "This is like the individual tree in the random forest.\n",
    "You train a different tree on each batch, and the overall model becomes the combination of the different decision trees (i.e. the forest).\n",
    "\n",
    "### Activation Amplitude\n",
    "\n",
    "If half the activations are missing during training then what happens when you turn off dropout?\n",
    "There would be twice as many input activations for each output activation!\n",
    "This would clearly affect the results.\n",
    "\n",
    "The fix for this is quite simple, and it can be applied at training time or at test time.\n",
    "\n",
    "#### Training Time Fix (recommended)\n",
    "\n",
    "To counteract the effect of dropping out some of the activations you increase the remaining activations by the reciprocal of the dropout probability.\n",
    "\n",
    "![training time augmentation](images/training-time-augmentation.jpg)\n",
    "\n",
    "#### Test Time Fix\n",
    "\n",
    "Or you can decrease the activations at test time by the dropout probability.\n",
    "\n",
    "![test time augmentation](images/test-time-augmentation.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Drop Connect\n",
    "\n",
    "This is like dropout except you apply it to the parameters instead of the activations.\n",
    "\n",
    "![drop connect](images/drop-connect.png)\n",
    "\n",
    "The justification behind this is that dropping activations leads to a regular pattern of missing data in the subsequent layer, while dropping individual weights randomizes that.\n",
    "AFAIK drop connect is less fashionable than dropout."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Weight Decay\n",
    "\n",
    "A parameter that receives constant positive reinforcement can increase until it is massive.\n",
    "It can then dominate the output activation whenever it receives input.\n",
    "This is symptomatic of overfitting.\n",
    "\n",
    "Weight decay encourages the network to find a solution that involves smaller parameters by making the parameter size a factor in the loss calculation.\n",
    "It's quite simple - you sum the square of every parameter in the network and multiply that by a factor, and then you can add that to the loss function:\n",
    "\n",
    "$$\n",
    "loss = loss_{og} + \\beta \\displaystyle\\sum_{i=1}^{parameters} W_i^2\n",
    "$$\n",
    "\n",
    "Using the square of the weight punishes large weights disproportionately.\n",
    "Ultimately the weights are used to encode the classification system that the network performs, so having non zero weights is desirable.\n",
    "\n",
    "![unit sphere](images/unit-sphere.png)\n",
    "\n",
    "(weight decay is _a bit_ like the above - it doesn't force all the weights to a unit length though!)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## DO IT\n",
    "\n",
    "So lets try and apply this stuff.\n",
    "\n",
    "Instead of doubling down on my insane composition stuff from notebook 1 I am going to keep things simple.\n",
    "\n",
    "Lets start with a function that just calculates the xor without regularization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import annotations\n",
    "from typing import *\n",
    "import itertools\n",
    "from functools import partial\n",
    "\n",
    "import jax\n",
    "import jax.numpy as np\n",
    "import jax.nn as nn\n",
    "# Current convention is to import original numpy as \"onp\"\n",
    "import numpy as onp\n",
    "\n",
    "import pandas as pd\n",
    "from tqdm import tqdm # notebook version is broken"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Tensor = np.ndarray\n",
    "Parameters = List[Tensor]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def xor_no_regularization(params: Parameters, x: Tensor) -> Tensor:\n",
    "    w1, b1, w2, b2 = params\n",
    "\n",
    "    x = np.dot(w1, x) + b1\n",
    "    x = np.tanh(x)\n",
    "    x = np.dot(w2, x) + b2\n",
    "    x = nn.sigmoid(x)\n",
    "\n",
    "    return x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now want a stricter training regimen, where we will establish that the outputs are 90% correct (rather than being just enough to round correctly).\n",
    "This will make the network train for longer and then we can test the effects of the regularization more easily.\n",
    "\n",
    "I do expect that regularizing such a small network will have little effect, and may even make training longer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])\n",
    "outputs = np.array([onp.bitwise_xor(*x) for x in inputs])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def initial_params():\n",
    "    return [\n",
    "        onp.random.randn(3, 2),  # w1\n",
    "        onp.random.randn(3),  # b1\n",
    "        onp.random.randn(3),  # w2\n",
    "        onp.random.randn(),  #b2\n",
    "    ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def strict_test(y_hat: Tensor, y: Tensor, threshold: float = 0.1) -> bool:\n",
    "    return np.abs(y_hat - y).max() <= 0.1\n",
    "\n",
    "def test_all_inputs(\n",
    "    net: Callable[[Parameters, Tensor], Tensor], params: Parameters, xs: List[Tensor], ys: List[Tensor], threshold: float = 0.1\n",
    ") -> bool:\n",
    "    return all(\n",
    "        strict_test(net(params, x), y, threshold)\n",
    "        for x, y in zip(xs, ys)\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def loss(net: Callable[[Parameters, Tensor], Tensor], params: Parameters, x: Tensor, y: Tensor) -> Tensor:\n",
    "    out = net(params, x)\n",
    "    cross_entropy = -y * np.log(out) - (1 - y) * np.log(1 - out)\n",
    "    return cross_entropy\n",
    "\n",
    "loss_grad = jax.grad(loss, argnums=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train(\n",
    "    train_net: Callable[[Parameters, Tensor], Tensor],\n",
    "    test_net: Callable[[Parameters, Tensor], Tensor],\n",
    "    params: Parameters,\n",
    "    xs: np.array,\n",
    "    ys: np.array,\n",
    "    learning_rate: float = 1.\n",
    ") -> int:\n",
    "    for n in itertools.count():\n",
    "        # Grab a single random training pair\n",
    "        idx = onp.random.choice(xs.shape[0])\n",
    "        x = xs[idx]\n",
    "        y = ys[idx]\n",
    "\n",
    "        # Get the gradient of the loss for this input/output pair\n",
    "        grads = loss_grad(train_net, params, x, y)\n",
    "\n",
    "        # Update parameters via gradient descent\n",
    "        params = [param - learning_rate * grad\n",
    "                  for param, grad in zip(params, grads)]\n",
    "        \n",
    "        # check accuracy every time, to get an accurate loop count\n",
    "        if test_all_inputs(test_net, params, xs, ys):\n",
    "            print(f'Training completed after {n} iterations')\n",
    "            return n\n",
    "        if n % 100 == 0:\n",
    "            print(f'Trained for {n} iterations without solving')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train(xor_no_regularization, xor_no_regularization, initial_params(), inputs, outputs) ;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I've run this a few times and got values between 98 and 198.\n",
    "\n",
    "Lets try with dropout now.\n",
    "I will apply the train time augmentation to the activations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dropout(activation: Tensor, dropout_rate: float = 0.5) -> Tensor:\n",
    "    # Create an array of the given shape and populate it with random samples from a uniform distribution over [0, 1).\n",
    "    mask = onp.random.rand(*activation.shape)\n",
    "    mask = mask > dropout_rate\n",
    "    mask = mask / (1 - dropout_rate) # train time augmentation\n",
    "    activation = activation * mask\n",
    "    \n",
    "    return activation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def xor_dropout(params: Parameters, x: Tensor, dropout_rate: float = 0.5) -> Tensor:\n",
    "    w1, b1, w2, b2 = params\n",
    "\n",
    "    # it is not fashionable to apply dropout to the inputs\n",
    "    x = np.dot(w1, x) + b1\n",
    "    x = np.tanh(x)\n",
    "    x = dropout(x, dropout_rate) # <-\n",
    "    x = np.dot(w2, x) + b2\n",
    "    x = nn.sigmoid(x)\n",
    "\n",
    "    return x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# train(xor_dropout, xor_no_regularization, initial_params(), inputs, outputs) ;\n",
    "# this did not converge after 5,300 iterations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train(partial(xor_dropout, dropout_rate=0.3), xor_no_regularization, initial_params(), inputs, outputs) ;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train(partial(xor_dropout, dropout_rate=0.1), xor_no_regularization, initial_params(), inputs, outputs) ;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The problem here is that really the network is too small to benefit from dropout - there is very little chance of overfitting to begin with.\n",
    "The harder problem, that has the larger network, is fizzbuzz, so lets try that now."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fizzbuzz_no_regularization(params: Parameters, x: Tensor) -> Tensor:\n",
    "    w1, b1, w2, b2 = params\n",
    "\n",
    "    x = np.dot(w1, x) + b1\n",
    "    x = np.tanh(x)\n",
    "    x = np.dot(w2, x) + b2\n",
    "\n",
    "    return x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fizzbuzz_initial_params():\n",
    "    return [\n",
    "        onp.random.randn(50, 10),  # w1\n",
    "        onp.random.randn(50),      # b1\n",
    "        onp.random.randn(4, 50),   # w2\n",
    "        onp.random.randn(4),       # b2\n",
    "    ]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fizz_buzz_encode(x: int) -> List[int]:\n",
    "    if x % 15 == 0:\n",
    "        return [0, 0, 0, 1]\n",
    "    elif x % 5 == 0:\n",
    "        return [0, 0, 1, 0]\n",
    "    elif x % 3 == 0:\n",
    "        return [0, 1, 0, 0]\n",
    "    else:\n",
    "        return [1, 0, 0, 0]\n",
    "\n",
    "def binary_encode(x: int) -> List[int]:\n",
    "    \"\"\"\n",
    "    10 digit binary encoding of x\n",
    "    \"\"\"\n",
    "    return [x >> i & 1 for i in range(10)]\n",
    "\n",
    "fizzbuzz_train_inputs = np.array([\n",
    "    binary_encode(x)\n",
    "    for x in range(101, 1024)\n",
    "])\n",
    "\n",
    "fizzbuzz_train_targets = np.array([\n",
    "    fizz_buzz_encode(x)\n",
    "    for x in range(101, 1024)\n",
    "])\n",
    "\n",
    "fizzbuzz_test_inputs = np.array([\n",
    "    binary_encode(x)\n",
    "    for x in range(1, 101)\n",
    "])\n",
    "\n",
    "fizzbuzz_test_targets = np.array([\n",
    "    fizz_buzz_encode(x)\n",
    "    for x in range(1, 101)\n",
    "])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fizzbuzz_loss(net: Callable[[Parameters, Tensor], Tensor], params: Parameters, x: Tensor, y: Tensor) -> Tensor:\n",
    "    out = net(params, x)\n",
    "    if any(x != x for x in out):\n",
    "        raise ValueError(f\"training has broken: {out}\")\n",
    "    cross_entropy = -np.sum(y * np.log(out) - (1 - y) * np.log(1 - out))\n",
    "    return cross_entropy\n",
    "\n",
    "fizzbuzz_loss_grad = jax.grad(fizzbuzz_loss, argnums=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train(\n",
    "    net: Callable[[Parameters, Tensor], Tensor],\n",
    "    params: Parameters,\n",
    "    xs: np.array,\n",
    "    ys: np.array,\n",
    "    learning_rate: float = 1.,\n",
    "    epochs: int = 5_000,\n",
    ") -> Parameters:\n",
    "    for n in tqdm(range(epochs)):\n",
    "        # Grab a single random training pair\n",
    "        idx = onp.random.choice(xs.shape[0])\n",
    "        x = xs[idx]\n",
    "        y = ys[idx]\n",
    "\n",
    "        # Get the gradient of the loss for this input/output pair\n",
    "        grads = fizzbuzz_loss_grad(net, params, x, y)\n",
    "        \n",
    "        # Update parameters via gradient descent\n",
    "        params = [param - learning_rate * grad\n",
    "                  for param, grad in zip(params, grads)]\n",
    "    return params\n",
    "\n",
    "def accuracy(\n",
    "    net: Callable[[Parameters, Tensor], Tensor],\n",
    "    params: Parameters,\n",
    "    xs: np.array,\n",
    "    ys: np.array,\n",
    ") -> float:\n",
    "    return sum(\n",
    "        np.argmax(net(params, np.array(binary_encode(x)))) == np.argmax(np.array(fizz_buzz_encode(x)))\n",
    "        for x in range(1, 101)\n",
    "    ) / 100"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params = train(\n",
    "    fizzbuzz_no_regularization,\n",
    "    fizzbuzz_initial_params(),\n",
    "    fizzbuzz_train_inputs,\n",
    "    fizzbuzz_train_targets,\n",
    "    learning_rate=0.001\n",
    ")\n",
    "accuracy(\n",
    "    fizzbuzz_no_regularization,\n",
    "    params,\n",
    "    fizzbuzz_test_inputs,\n",
    "    fizzbuzz_test_targets,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "for x in range(1, 101):\n",
    "    predicted = fizzbuzz_no_regularization(params, np.array(binary_encode(x)))\n",
    "    predicted_idx = np.argmax(predicted)\n",
    "    actual_idx = np.argmax(np.array(fizz_buzz_encode(x)))\n",
    "    labels = [str(x), \"fizz\", \"buzz\", \"fizzbuzz\"]\n",
    "    print(f\"{x:>4}: {labels[predicted_idx]:>8} {labels[actual_idx]:>8}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dropout(activation: Tensor, dropout_rate: float = 0.5) -> Tensor:\n",
    "    # Create an array of the given shape and populate it with random samples from a uniform distribution over [0, 1).\n",
    "    mask = onp.random.rand(*activation.shape)\n",
    "    mask = mask > dropout_rate\n",
    "    # mask = mask / (1 - dropout_rate) # train time augmentation\n",
    "    # doing this was making it break :(\n",
    "    activation = activation * mask\n",
    "    \n",
    "    return activation\n",
    "\n",
    "def fizzbuzz_dropout(params: Parameters, x: Tensor, dropout_rate: float = 0.5) -> Tensor:\n",
    "    w1, b1, w2, b2 = params\n",
    "\n",
    "    x = np.dot(w1, x) + b1\n",
    "    x = np.tanh(x)\n",
    "    x = dropout(x, dropout_rate) # <-\n",
    "    x = np.dot(w2, x) + b2\n",
    "    x = nn.softmax(x)\n",
    "\n",
    "    return x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params = train(\n",
    "    fizzbuzz_dropout,\n",
    "    fizzbuzz_initial_params(),\n",
    "    fizzbuzz_train_inputs,\n",
    "    fizzbuzz_train_targets,\n",
    "    learning_rate=0.001,\n",
    ")\n",
    "accuracy(\n",
    "    fizzbuzz_no_regularization,\n",
    "    params,\n",
    "    fizzbuzz_test_inputs,\n",
    "    fizzbuzz_test_targets,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "for x in range(1, 101):\n",
    "    predicted = fizzbuzz_no_regularization(params, np.array(binary_encode(x)))\n",
    "    predicted_idx = np.argmax(predicted)\n",
    "    actual_idx = np.argmax(np.array(fizz_buzz_encode(x)))\n",
    "    labels = [str(x), \"fizz\", \"buzz\", \"fizzbuzz\"]\n",
    "    print(f\"{x:>4}: {labels[predicted_idx]:>8} {labels[actual_idx]:>8}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So fizzbuzz is going really badly.\n",
    "It seems that one or two output classes dominate the results.\n",
    "I wonder if it needs more training (joelnet only needed 5k)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params = train(\n",
    "    fizzbuzz_dropout,\n",
    "    fizzbuzz_initial_params(),\n",
    "    fizzbuzz_train_inputs,\n",
    "    fizzbuzz_train_targets,\n",
    "    learning_rate=0.001,\n",
    "    epochs=10_000\n",
    ")\n",
    "accuracy(\n",
    "    fizzbuzz_no_regularization,\n",
    "    params,\n",
    "    fizzbuzz_test_inputs,\n",
    "    fizzbuzz_test_targets,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's barely better after training longer.\n",
    "I'll just have to live with this.\n",
    "Maybe Colin has a solution.\n",
    "\n",
    "Next up is weight decay."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fizzbuzz_loss(net: Callable[[Parameters, Tensor], Tensor], params: Parameters, x: Tensor, y: Tensor, wd: float = 0.) -> Tensor:\n",
    "    out = net(params, x)\n",
    "    if any(x != x for x in out):\n",
    "        raise ValueError(f\"training has broken: {out}\")\n",
    "    cross_entropy = -np.sum(y * np.log(out) - (1 - y) * np.log(1 - out))\n",
    "    weight_decay = wd * sum((p*p).sum() for p in params)\n",
    "    return cross_entropy + weight_decay\n",
    "\n",
    "fizzbuzz_loss_grad = jax.grad(fizzbuzz_loss, argnums=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train(\n",
    "    net: Callable[[Parameters, Tensor], Tensor],\n",
    "    params: Parameters,\n",
    "    xs: np.array,\n",
    "    ys: np.array,\n",
    "    learning_rate: float = 1.,\n",
    "    epochs: int = 5_000,\n",
    "    loss: Callable[[Callable[[Parameters, Tensor], Tensor], Parameters, Tensor, Tensor]] = fizzbuzz_loss\n",
    ") -> Parameters:\n",
    "    grad_loss = jax.grad(loss, argnums=1)\n",
    "    for n in tqdm(range(epochs)):\n",
    "        # Grab a single random training pair\n",
    "        idx = onp.random.choice(xs.shape[0])\n",
    "        x = xs[idx]\n",
    "        y = ys[idx]\n",
    "\n",
    "        # Get the gradient of the loss for this input/output pair\n",
    "        grads = grad_loss(net, params, x, y)\n",
    "        \n",
    "        # Update parameters via gradient descent\n",
    "        params = [param - learning_rate * grad\n",
    "                  for param, grad in zip(params, grads)]\n",
    "    return params\n",
    "\n",
    "def accuracy(\n",
    "    net: Callable[[Parameters, Tensor], Tensor],\n",
    "    params: Parameters,\n",
    "    xs: np.array,\n",
    "    ys: np.array,\n",
    ") -> float:\n",
    "    return sum(\n",
    "        np.argmax(net(params, np.array(binary_encode(x)))) == np.argmax(np.array(fizz_buzz_encode(x)))\n",
    "        for x in range(1, 101)\n",
    "    ) / 100"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params = train(\n",
    "    fizzbuzz_no_regularization,\n",
    "    fizzbuzz_initial_params(),\n",
    "    fizzbuzz_train_inputs,\n",
    "    fizzbuzz_train_targets,\n",
    "    learning_rate=0.001,\n",
    "    loss=partial(fizzbuzz_loss, wd=0.0001)\n",
    ")\n",
    "accuracy(\n",
    "    fizzbuzz_no_regularization,\n",
    "    params,\n",
    "    fizzbuzz_test_inputs,\n",
    "    fizzbuzz_test_targets,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params = train(\n",
    "    fizzbuzz_dropout,\n",
    "    fizzbuzz_initial_params(),\n",
    "    fizzbuzz_train_inputs,\n",
    "    fizzbuzz_train_targets,\n",
    "    learning_rate=0.001,\n",
    "    loss=partial(fizzbuzz_loss, wd=0.0001)\n",
    ")\n",
    "accuracy(\n",
    "    fizzbuzz_no_regularization,\n",
    "    params,\n",
    "    fizzbuzz_test_inputs,\n",
    "    fizzbuzz_test_targets,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results so far:\n",
    "\n",
    "regularization           | epochs | lr    | accuracy\n",
    "-------------------------|--------|-------|---------\n",
    "none                     | 5,000  | 0.001 | 0.21\n",
    "dropout                  | 5,000  | 0.001 | 0.42\n",
    "dropout                  | 10,000 | 0.001 | 0.49\n",
    "weight decay             | 5,000  | 0.001 | 0.13\n",
    "dropout and weight decay | 5,000  | 0.001 | 0.44\n",
    "\n",
    "I ran the weight decay training a second time and it got 0.53 accuracy.\n",
    "It predicts the number 100% of the time, which is the optimal lazy strategy.\n",
    "\n",
    "I was disappointed that my dropout implementation caused the activations to break."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
